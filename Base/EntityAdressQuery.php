<?php

namespace Base;

use \EntityAdress as ChildEntityAdress;
use \EntityAdressQuery as ChildEntityAdressQuery;
use \Exception;
use Map\EntityAdressTableMap;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\ActiveQuery\ModelJoin;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\LogicException;
use Propel\Runtime\Exception\PropelException;

/**
 * Base class that represents a query for the 'entity_adress' table.
 *
 *
 *
 * @method     ChildEntityAdressQuery orderByEntityId($order = Criteria::ASC) Order by the entity_id column
 * @method     ChildEntityAdressQuery orderByAddressId($order = Criteria::ASC) Order by the address_id column
 * @method     ChildEntityAdressQuery orderByEntityType($order = Criteria::ASC) Order by the entity_type column
 * @method     ChildEntityAdressQuery orderByAdditionalInfo($order = Criteria::ASC) Order by the additional_info column
 *
 * @method     ChildEntityAdressQuery groupByEntityId() Group by the entity_id column
 * @method     ChildEntityAdressQuery groupByAddressId() Group by the address_id column
 * @method     ChildEntityAdressQuery groupByEntityType() Group by the entity_type column
 * @method     ChildEntityAdressQuery groupByAdditionalInfo() Group by the additional_info column
 *
 * @method     ChildEntityAdressQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method     ChildEntityAdressQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method     ChildEntityAdressQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method     ChildEntityAdressQuery leftJoinWith($relation) Adds a LEFT JOIN clause and with to the query
 * @method     ChildEntityAdressQuery rightJoinWith($relation) Adds a RIGHT JOIN clause and with to the query
 * @method     ChildEntityAdressQuery innerJoinWith($relation) Adds a INNER JOIN clause and with to the query
 *
 * @method     ChildEntityAdressQuery leftJoinUser($relationAlias = null) Adds a LEFT JOIN clause to the query using the User relation
 * @method     ChildEntityAdressQuery rightJoinUser($relationAlias = null) Adds a RIGHT JOIN clause to the query using the User relation
 * @method     ChildEntityAdressQuery innerJoinUser($relationAlias = null) Adds a INNER JOIN clause to the query using the User relation
 *
 * @method     ChildEntityAdressQuery joinWithUser($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the User relation
 *
 * @method     ChildEntityAdressQuery leftJoinWithUser() Adds a LEFT JOIN clause and with to the query using the User relation
 * @method     ChildEntityAdressQuery rightJoinWithUser() Adds a RIGHT JOIN clause and with to the query using the User relation
 * @method     ChildEntityAdressQuery innerJoinWithUser() Adds a INNER JOIN clause and with to the query using the User relation
 *
 * @method     ChildEntityAdressQuery leftJoinAdress($relationAlias = null) Adds a LEFT JOIN clause to the query using the Adress relation
 * @method     ChildEntityAdressQuery rightJoinAdress($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Adress relation
 * @method     ChildEntityAdressQuery innerJoinAdress($relationAlias = null) Adds a INNER JOIN clause to the query using the Adress relation
 *
 * @method     ChildEntityAdressQuery joinWithAdress($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the Adress relation
 *
 * @method     ChildEntityAdressQuery leftJoinWithAdress() Adds a LEFT JOIN clause and with to the query using the Adress relation
 * @method     ChildEntityAdressQuery rightJoinWithAdress() Adds a RIGHT JOIN clause and with to the query using the Adress relation
 * @method     ChildEntityAdressQuery innerJoinWithAdress() Adds a INNER JOIN clause and with to the query using the Adress relation
 *
 * @method     \UserQuery|\AdressQuery endUse() Finalizes a secondary criteria and merges it with its primary Criteria
 *
 * @method     ChildEntityAdress findOne(ConnectionInterface $con = null) Return the first ChildEntityAdress matching the query
 * @method     ChildEntityAdress findOneOrCreate(ConnectionInterface $con = null) Return the first ChildEntityAdress matching the query, or a new ChildEntityAdress object populated from the query conditions when no match is found
 *
 * @method     ChildEntityAdress findOneByEntityId(int $entity_id) Return the first ChildEntityAdress filtered by the entity_id column
 * @method     ChildEntityAdress findOneByAddressId(int $address_id) Return the first ChildEntityAdress filtered by the address_id column
 * @method     ChildEntityAdress findOneByEntityType(int $entity_type) Return the first ChildEntityAdress filtered by the entity_type column
 * @method     ChildEntityAdress findOneByAdditionalInfo(string $additional_info) Return the first ChildEntityAdress filtered by the additional_info column *

 * @method     ChildEntityAdress requirePk($key, ConnectionInterface $con = null) Return the ChildEntityAdress by primary key and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildEntityAdress requireOne(ConnectionInterface $con = null) Return the first ChildEntityAdress matching the query and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildEntityAdress requireOneByEntityId(int $entity_id) Return the first ChildEntityAdress filtered by the entity_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildEntityAdress requireOneByAddressId(int $address_id) Return the first ChildEntityAdress filtered by the address_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildEntityAdress requireOneByEntityType(int $entity_type) Return the first ChildEntityAdress filtered by the entity_type column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildEntityAdress requireOneByAdditionalInfo(string $additional_info) Return the first ChildEntityAdress filtered by the additional_info column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildEntityAdress[]|ObjectCollection find(ConnectionInterface $con = null) Return ChildEntityAdress objects based on current ModelCriteria
 * @method     ChildEntityAdress[]|ObjectCollection findByEntityId(int $entity_id) Return ChildEntityAdress objects filtered by the entity_id column
 * @method     ChildEntityAdress[]|ObjectCollection findByAddressId(int $address_id) Return ChildEntityAdress objects filtered by the address_id column
 * @method     ChildEntityAdress[]|ObjectCollection findByEntityType(int $entity_type) Return ChildEntityAdress objects filtered by the entity_type column
 * @method     ChildEntityAdress[]|ObjectCollection findByAdditionalInfo(string $additional_info) Return ChildEntityAdress objects filtered by the additional_info column
 * @method     ChildEntityAdress[]|\Propel\Runtime\Util\PropelModelPager paginate($page = 1, $maxPerPage = 10, ConnectionInterface $con = null) Issue a SELECT query based on the current ModelCriteria and uses a page and a maximum number of results per page to compute an offset and a limit
 *
 */
abstract class EntityAdressQuery extends ModelCriteria
{
    protected $entityNotFoundExceptionClass = '\\Propel\\Runtime\\Exception\\EntityNotFoundException';

    /**
     * Initializes internal state of \Base\EntityAdressQuery object.
     *
     * @param     string $dbName The database name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'default', $modelName = '\\EntityAdress', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new ChildEntityAdressQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param     Criteria $criteria Optional Criteria to build the query from
     *
     * @return ChildEntityAdressQuery
     */
    public static function create($modelAlias = null, Criteria $criteria = null)
    {
        if ($criteria instanceof ChildEntityAdressQuery) {
            return $criteria;
        }
        $query = new ChildEntityAdressQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param ConnectionInterface $con an optional connection object
     *
     * @return ChildEntityAdress|array|mixed the result, formatted by the current formatter
     */
    public function findPk($key, ConnectionInterface $con = null)
    {
        throw new LogicException('The EntityAdress object has no primary key');
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(array(12, 56), array(832, 123), array(123, 456)), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     ConnectionInterface $con an optional connection object
     *
     * @return ObjectCollection|array|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, ConnectionInterface $con = null)
    {
        throw new LogicException('The EntityAdress object has no primary key');
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return $this|ChildEntityAdressQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {
        throw new LogicException('The EntityAdress object has no primary key');
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return $this|ChildEntityAdressQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {
        throw new LogicException('The EntityAdress object has no primary key');
    }

    /**
     * Filter the query on the entity_id column
     *
     * Example usage:
     * <code>
     * $query->filterByEntityId(1234); // WHERE entity_id = 1234
     * $query->filterByEntityId(array(12, 34)); // WHERE entity_id IN (12, 34)
     * $query->filterByEntityId(array('min' => 12)); // WHERE entity_id > 12
     * </code>
     *
     * @see       filterByUser()
     *
     * @param     mixed $entityId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildEntityAdressQuery The current query, for fluid interface
     */
    public function filterByEntityId($entityId = null, $comparison = null)
    {
        if (is_array($entityId)) {
            $useMinMax = false;
            if (isset($entityId['min'])) {
                $this->addUsingAlias(EntityAdressTableMap::COL_ENTITY_ID, $entityId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($entityId['max'])) {
                $this->addUsingAlias(EntityAdressTableMap::COL_ENTITY_ID, $entityId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(EntityAdressTableMap::COL_ENTITY_ID, $entityId, $comparison);
    }

    /**
     * Filter the query on the address_id column
     *
     * Example usage:
     * <code>
     * $query->filterByAddressId(1234); // WHERE address_id = 1234
     * $query->filterByAddressId(array(12, 34)); // WHERE address_id IN (12, 34)
     * $query->filterByAddressId(array('min' => 12)); // WHERE address_id > 12
     * </code>
     *
     * @see       filterByAdress()
     *
     * @param     mixed $addressId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildEntityAdressQuery The current query, for fluid interface
     */
    public function filterByAddressId($addressId = null, $comparison = null)
    {
        if (is_array($addressId)) {
            $useMinMax = false;
            if (isset($addressId['min'])) {
                $this->addUsingAlias(EntityAdressTableMap::COL_ADDRESS_ID, $addressId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($addressId['max'])) {
                $this->addUsingAlias(EntityAdressTableMap::COL_ADDRESS_ID, $addressId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(EntityAdressTableMap::COL_ADDRESS_ID, $addressId, $comparison);
    }

    /**
     * Filter the query on the entity_type column
     *
     * Example usage:
     * <code>
     * $query->filterByEntityType(1234); // WHERE entity_type = 1234
     * $query->filterByEntityType(array(12, 34)); // WHERE entity_type IN (12, 34)
     * $query->filterByEntityType(array('min' => 12)); // WHERE entity_type > 12
     * </code>
     *
     * @see       filterByUser()
     *
     * @param     mixed $entityType The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildEntityAdressQuery The current query, for fluid interface
     */
    public function filterByEntityType($entityType = null, $comparison = null)
    {
        if (is_array($entityType)) {
            $useMinMax = false;
            if (isset($entityType['min'])) {
                $this->addUsingAlias(EntityAdressTableMap::COL_ENTITY_TYPE, $entityType['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($entityType['max'])) {
                $this->addUsingAlias(EntityAdressTableMap::COL_ENTITY_TYPE, $entityType['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(EntityAdressTableMap::COL_ENTITY_TYPE, $entityType, $comparison);
    }

    /**
     * Filter the query on the additional_info column
     *
     * Example usage:
     * <code>
     * $query->filterByAdditionalInfo('fooValue');   // WHERE additional_info = 'fooValue'
     * $query->filterByAdditionalInfo('%fooValue%', Criteria::LIKE); // WHERE additional_info LIKE '%fooValue%'
     * </code>
     *
     * @param     string $additionalInfo The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildEntityAdressQuery The current query, for fluid interface
     */
    public function filterByAdditionalInfo($additionalInfo = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($additionalInfo)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(EntityAdressTableMap::COL_ADDITIONAL_INFO, $additionalInfo, $comparison);
    }

    /**
     * Filter the query by a related \User object
     *
     * @param \User $user The related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildEntityAdressQuery The current query, for fluid interface
     */
    public function filterByUser($user, $comparison = null)
    {
        if ($user instanceof \User) {
            return $this
                ->addUsingAlias(EntityAdressTableMap::COL_ENTITY_ID, $user->getId(), $comparison)
                ->addUsingAlias(EntityAdressTableMap::COL_ENTITY_TYPE, 'Usuario', $comparison);
        } else {
            throw new PropelException('filterByUser() only accepts arguments of type \User');
        }
    }

    /**
     * Adds a JOIN clause to the query using the User relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildEntityAdressQuery The current query, for fluid interface
     */
    public function joinUser($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('User');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'User');
        }

        return $this;
    }

    /**
     * Use the User relation User object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \UserQuery A secondary query class using the current class as primary query
     */
    public function useUserQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinUser($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'User', '\UserQuery');
    }

    /**
     * Filter the query by a related \Adress object
     *
     * @param \Adress|ObjectCollection $adress The related object(s) to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildEntityAdressQuery The current query, for fluid interface
     */
    public function filterByAdress($adress, $comparison = null)
    {
        if ($adress instanceof \Adress) {
            return $this
                ->addUsingAlias(EntityAdressTableMap::COL_ADDRESS_ID, $adress->getId(), $comparison);
        } elseif ($adress instanceof ObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(EntityAdressTableMap::COL_ADDRESS_ID, $adress->toKeyValue('PrimaryKey', 'Id'), $comparison);
        } else {
            throw new PropelException('filterByAdress() only accepts arguments of type \Adress or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Adress relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildEntityAdressQuery The current query, for fluid interface
     */
    public function joinAdress($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Adress');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Adress');
        }

        return $this;
    }

    /**
     * Use the Adress relation Adress object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \AdressQuery A secondary query class using the current class as primary query
     */
    public function useAdressQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinAdress($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Adress', '\AdressQuery');
    }

    /**
     * Exclude object from result
     *
     * @param   ChildEntityAdress $entityAdress Object to remove from the list of results
     *
     * @return $this|ChildEntityAdressQuery The current query, for fluid interface
     */
    public function prune($entityAdress = null)
    {
        if ($entityAdress) {
            throw new LogicException('EntityAdress object has no primary key');

        }

        return $this;
    }

    /**
     * Deletes all rows from the entity_adress table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public function doDeleteAll(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(EntityAdressTableMap::DATABASE_NAME);
        }

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con) {
            $affectedRows = 0; // initialize var to track total num of affected rows
            $affectedRows += parent::doDeleteAll($con);
            // Because this db requires some delete cascade/set null emulation, we have to
            // clear the cached instance *after* the emulation has happened (since
            // instances get re-added by the select statement contained therein).
            EntityAdressTableMap::clearInstancePool();
            EntityAdressTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    /**
     * Performs a DELETE on the database based on the current ModelCriteria
     *
     * @param ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public function delete(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(EntityAdressTableMap::DATABASE_NAME);
        }

        $criteria = $this;

        // Set the correct dbName
        $criteria->setDbName(EntityAdressTableMap::DATABASE_NAME);

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con, $criteria) {
            $affectedRows = 0; // initialize var to track total num of affected rows

            EntityAdressTableMap::removeInstanceFromPool($criteria);

            $affectedRows += ModelCriteria::delete($con);
            EntityAdressTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

} // EntityAdressQuery
