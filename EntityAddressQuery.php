<?php

use Base\EntityAddressQuery as BaseEntityAddressQuery;

/**
 * Skeleton subclass for performing query and update operations on the 'entity_address' table.
 *
 *
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 */
class EntityAddressQuery extends BaseEntityAddressQuery
{

}
