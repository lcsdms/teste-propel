<?php

use Base\EntityAdress as BaseEntityAdress;

/**
 * Skeleton subclass for representing a row from the 'entity_adress' table.
 *
 *
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 */
class EntityAdress extends BaseEntityAdress
{

}
