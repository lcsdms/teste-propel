
# This is a fix for InnoDB in MySQL >= 4.1.x
# It "suspends judgement" for fkey relationships until are tables are set.
SET FOREIGN_KEY_CHECKS = 0;

-- ---------------------------------------------------------------------
-- user
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `user`;

CREATE TABLE `user`
(
    `id` INTEGER NOT NULL AUTO_INCREMENT,
    `nome` VARCHAR(200) NOT NULL,
    PRIMARY KEY (`id`)
) ENGINE=InnoDB;

-- ---------------------------------------------------------------------
-- company
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `company`;

CREATE TABLE `company`
(
    `id` INTEGER NOT NULL AUTO_INCREMENT,
    `nome` VARCHAR(200) NOT NULL,
    PRIMARY KEY (`id`)
) ENGINE=InnoDB;

-- ---------------------------------------------------------------------
-- address
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `address`;

CREATE TABLE `address`
(
    `id` INTEGER NOT NULL AUTO_INCREMENT,
    `street` VARCHAR(200) NOT NULL,
    PRIMARY KEY (`id`)
) ENGINE=InnoDB;

-- ---------------------------------------------------------------------
-- entity_address
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `entity_address`;

CREATE TABLE `entity_address`
(
    `entity_id` INTEGER NOT NULL,
    `address_id` INTEGER NOT NULL,
    `entity_type` VARCHAR(100) NOT NULL,
    `additional_info` VARCHAR(255),
    PRIMARY KEY (`entity_id`,`address_id`,`entity_type`),
    UNIQUE INDEX `entity_address_u_1c1447` (`entity_id`, `entity_type`, `address_id`),
    INDEX `entity_address_fi_a7b49f` (`address_id`),
    CONSTRAINT `entity_address_fk_a7b49f`
        FOREIGN KEY (`address_id`)
        REFERENCES `address` (`id`)
) ENGINE=InnoDB;

# This restores the fkey checks, after having unset them earlier
SET FOREIGN_KEY_CHECKS = 1;
