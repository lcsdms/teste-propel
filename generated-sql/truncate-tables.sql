/**
Limpeza de tabelas de teste
 */
SET FOREIGN_KEY_CHECKS = 0;
TRUNCATE TABLE `teste-propel`.entity_address;
TRUNCATE TABLE `teste-propel`.address;
TRUNCATE TABLE `teste-propel`.user;
TRUNCATE TABLE `teste-propel`.company;
SET FOREIGN_KEY_CHECKS = 1;