<?php
return [
    'propel' => [
        'database' => [
            'connections' => [
                'default' => [
                    'adapter' => 'mysql',
                    'dsn' => 'mysql:host=localhost;port=3306;dbname=teste-propel',
                    'user' => 'root',
                    'password' => 'root',
                    'settings' => [
                        'charset' => 'utf8'
                    ],
                    'classname'  => 'Propel\Runtime\Connection\DebugPDO' //Habilita o Debug da aplicacao
                ]
            ]
        ]
    ]
];
