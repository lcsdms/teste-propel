<?php
require_once "vendor/autoload.php";
require_once "generated-conf/config.php";

//\Propel\Runtime\Propel::disableInstancePooling();
$rua = "Rua Marquesas, 10";
echo "Procurando usuario com endereco ".$rua.PHP_EOL;
//Procurando USUARIOS que tenham endereco com uma rua especifica.
$usuarioEncontrado = UserQuery::create()->useEntityAddressQuery()->useAddressQuery()->filterByStreet($rua)->endUse()->endUse()->findOne();

if(!$usuarioEncontrado){
    echo "Nenhum usuário encontrado para o endereço".PHP_EOL;
    die(0);
}
echo "Usuário encontrado: ".$usuarioEncontrado->getNome().PHP_EOL;

//Buscando os endereços do usuario encontrado:
$enderecos = $usuarioEncontrado->getEntityAddressesJoinAddress();
foreach ($enderecos as $endereco) {
    echo "Endereço: ".$endereco->getAddress()->getStreet()." - Informações complementares: ".$endereco->getAdditionalInfo().PHP_EOL;
}

echo "==========================================".PHP_EOL;
echo "Buscando Empresas que tenham endereço.... ".PHP_EOL;
echo "==========================================".PHP_EOL;
//Join usando texto escrito
$usuariosComEndereco = UserQuery::create()
    ->joinWith("User.EntityAddress")
    ->joinWith("EntityAddress.Address")
    ->find();

foreach ($usuariosComEndereco as $usuario){
    echo "Usuario: ". $usuario->getNome().PHP_EOL;
    $entidadeEnderecos = $usuario->getEntityAddresses();
    if($entidadeEnderecos == null){
        echo "Usuario sem Endereços";
    }else{
        echo "Endereços para o usuário ".$usuario->getNome().PHP_EOL;
        foreach ($entidadeEnderecos as $et) {
            echo "Street:" . $et->getAddress()->getStreet().PHP_EOL;
        }
    }
}
echo "==========================================".PHP_EOL;
echo "Buscando Empresas que tenham endereço.... ".PHP_EOL;
echo "==========================================".PHP_EOL;

$empresasComEndereco = CompanyQuery::create()->joinWithEntityAddress()->find();

foreach ($empresasComEndereco as $empresa) {
    echo "Empresa: ".$empresa->getNome().PHP_EOL;
    $entidadeEnderecos = $empresa->getEntityAddressesJoinAddress(null,null,'INNER JOIN');
//    $entidadeEnderecos = $empresa->getEntityAddressesJoinAddress(null,null,\Propel\Runtime\ActiveQuery\Criteria::INNER_JOIN);
    if($entidadeEnderecos == null){
        echo "Empresa sem Endereços";
    }else{
        echo "Endereços para a Empresa ".$empresa->getNome().PHP_EOL;
        foreach ($entidadeEnderecos as $et) {
            echo "ID Endereço: ".$et->getAddress()->getId().PHP_EOL;
            echo "Street:" . $et->getAddress()->getStreet().PHP_EOL;
        }
    }
}



//todo verificar os casos referente a metodos que nao referenciam as classes, quando mais de uma entidade utiliza uma relacao polimorfica, o metodo de join aparece para as outras tambem.
