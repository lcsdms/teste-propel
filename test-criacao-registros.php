<?php
require_once "vendor/autoload.php";
require_once "generated-conf/config.php";
//Adicionando um usuario
$user = new User();
$user->setNome("Lucas");
$address = new Address();
$address->setStreet("Rua Marquesas, 10");

// Adicionando dois enderecos
$address2 = new Address();
$address2->setStreet("Avenida Lins de Vasconcelos, 356");

$entityAddress = new EntityAddress();
$entityAddress->setAddress($address);

$entityAddress2 = new EntityAddress();
$entityAddress2->setAddress($address2);

$user->addEntityAddress($entityAddress);
$user->addEntityAddress($entityAddress2);

$user->save();

echo "Usuario Salvo Com sucesso".PHP_EOL;
echo "==============================".PHP_EOL;
echo "Criando uma empresa".PHP_EOL;
$empresa = new Company();
$empresa->setNome("Empresa Teste");

$address = new Address();
$address->setStreet("Endereço 1 Empresa");

// Adicionando dois enderecos
$address2 = new Address();
$address2->setStreet("Endereço 2 Empresa");

$entityAddress = new EntityAddress();
$entityAddress->setAddress($address);

$entityAddress2 = new EntityAddress();
$entityAddress2->setAddress($address2);

$empresa->addEntityAddress($entityAddress);
$empresa->addEntityAddress($entityAddress2);

$empresa->save();


