<?php
require_once "vendor/autoload.php";
require_once "generated-conf/config.php";

//echo "Tentando deletar usuário usando query sem where".PHP_EOL;
//$userQuery = new UserQuery();
//$userQuery->find()->delete();
//echo "Registros deletados com sucesso.".PHP_EOL;

//echo " ====== Tentando deletar um registro diretamente pelo model ========".PHP_EOL;
//$userQuery = new UserQuery();
//$usuario = $userQuery->findOneById(1);
//$usuario->delete();
//echo "Usuario Encontrado - Nome: ".$usuario->getNome().PHP_EOL;
//echo "Deletado com sucesso, nao limpou a tabela referencial".PHP_EOL;

//echo "Tentando deletar registro pela Query da tabela pivot (Entity_Address)".PHP_EOL;
//$entidadeAdressQuery = new EntityAddressQuery();
//$entidadeAdressQuery->find()->delete();
//echo "Deletado com sucesso".PHP_EOL;
//echo "Deletado com sucesso, mas nao alterou nenhuma outra tabela alem dela, mesmo com a trigger de delete".PHP_EOL;

//echo "Tentando deletar registro pela query da tabela de enderecos".PHP_EOL;
//$adressQuery = new AddressQuery();
//$adressQuery->find()->delete();
//echo "Deletado com sucesso! ".PHP_EOL;

//try {
//    echo "Tentando modificar registro pelos models da tabela de enderecos" . PHP_EOL;
//    $adressQuery = new AddressQuery();
//    $listaEndereco = $adressQuery->find();
//    foreach ($listaEndereco as $endereco) {
//        echo "Endereço atual:" . $endereco->getStreet() . PHP_EOL;
//        $endereco->setStreet('Modificando a rua para teste');
//        $endereco->save();
//        echo "Endereço modificado:" . $endereco->getStreet() . PHP_EOL;
//    }
//    echo "Endereços modificados com sucesso! " . PHP_EOL;
//    echo "Lista de endereços atuais: " . PHP_EOL;
//    foreach ($listaEndereco as $endereco) {
//        echo $endereco->getStreet() . PHP_EOL;
//    }
//    echo "Endereços modificados com sucesso" . PHP_EOL;
//} catch (Exception $e) {
//    echo "Erro ao tentar deletar os registros".PHP_EOL;
//    echo $e->getMessage();
//}



